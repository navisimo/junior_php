<?php

$digits = function(int $length)
{
    while ($length > 0) {
        yield mt_rand();
        --$length;
    }
};

class Log
{

    static function staticLog($logString)
    {
        $today = date("F j, Y, g:i:s a");
        $fp = fopen('test.txt', 'a+');
        $log = fwrite($fp, $today . "\r\n". $logString);
        fclose($fp);
    }

}

class Sequence extends SplMinHeap
{

    private $count;
    private $newArr;


    function __construct($m)
    {
        $this->count = $m;
    }

    public function add($number)
    {
        $nameFunc = 'add';

        $start = microtime(true);

        if($this->isEmpty() || $this->count() < $this->count) {
            $this->insert($number);
        } elseif($number > $this->top()) {
            $this->extract();
            $this->insert($number);
        }

        $end = microtime(true);

        Log::staticLog(self::formingString($nameFunc, $end, $start, $number));

    }


    public function getMaxNumbers()
    {
        $nameFunc = 'getMaxNumbers';

        $start = microtime(true);

        while ($this->valid()) {
            $this->newArr[] = $this->current();
            $this->next();
        }

        $end = microtime(true);

        $arrImplode = implode("\r\nnumber: ", $this->newArr);

        Log::staticLog(self::formingString($nameFunc, $end, $start, $arrImplode));

        return $this->newArr;

    }

    static function formingString($nameFunc, $end, $start, $number)
    {
        return 'Function ' . $nameFunc . ":\r\nScript run time: " . ($end - $start)
            . "\r\nMemory: " . memory_get_usage()
            . "\r\nnumber: " . $number
            . "\r\n\r\n";
    }

}

$sequence = new Sequence(5000);

foreach ($digits(100000) as $number) {
   $sequence->add($number);
}

print_r($sequence->getMaxNumbers());
